package algGeom.lib3D;

import Util.*;
import javax.media.opengl.GL;

public class DrawLine3d extends Draw {

    Line3d line;

    public DrawLine3d(Line3d e) {
        line = e;
    }

    public Line3d getLine() {
        return line;
    }

    @Override
    public void drawObject(GL g) {

        Vect3d low = line.getPoint(-1000);
        Vect3d high = line.getPoint(1000);

        g.glBegin(GL.GL_LINES);
        g.glVertex3d(low.x, low.y, low.z);
        g.glVertex3d(high.x, high.y, high.z);
        g.glEnd();
        
    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glColor3f(R, G, B);
        drawObject(g);

    }
}
