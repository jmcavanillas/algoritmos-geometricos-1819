package algGeom.lib3D;

import Util.*;
import javax.media.opengl.*;

public class DrawPlane extends Draw {

    Plane pl;

    public DrawPlane(Plane p) {
        pl = p;
    }

    public Plane getPlane() {
        return pl;
    }

    @Override
    public void drawObject(GL g) {
        Line3d l1 = new Line3d(pl.a, pl.b);
        Line3d l2 = new Line3d(pl.a, pl.c);

        Vect3d a = l1.getPoint(-50);
        Vect3d b = l1.getPoint(50);
        Vect3d c = l2.getPoint(-50);
        Vect3d d = l2.getPoint(50);

        Triangle3d t1 = new Triangle3d(a, c, b);
        DrawTriangle3d dt1 = new DrawTriangle3d(t1);
        dt1.drawObject(g);

        Triangle3d t2 = new Triangle3d(b, d, a);
        DrawTriangle3d dt2 = new DrawTriangle3d(t2);
        dt2.drawObject(g);
    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glColor3f(R, G, B);
        drawObject(g);
    }

    public void drawObjectC(GL g, float R, float G, float B, float A) {
        g.glColor4f(R, G, B, A);
        drawObject(g);
    }

}
