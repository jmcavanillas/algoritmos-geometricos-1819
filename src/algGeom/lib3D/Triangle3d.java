package algGeom.lib3D;

import Util.BasicGeom;
import Util.MollerTriAABB;
import algGeom.lib3D.Face.RelationTriPlane;

enum PointTrianglePosition {
    PARALELL, COLLINEAR, INTERSECTS, NO_INTERSECTS
};

public class Triangle3d {

    public enum PointPosition {
        POSITIVE, NEGATIVE, COPLANAR
    };

    /**
     * a triangle is defined by three points
     */
    protected Vect3d a, b, c;

    /**
     * default constructor with values (0,0,0)
     */
    public Triangle3d() {
        a = new Vect3d();
        b = new Vect3d();
        c = new Vect3d();
    }

    /**
     * constructor from their coordinates
     */
    public Triangle3d(double ax, double ay, double az,
            double bx, double by, double bz,
            double cx, double cy, double cz) {
        a = new Vect3d(ax, ay, az);
        b = new Vect3d(bx, by, bz);
        c = new Vect3d(cx, cy, cz);
    }

    /**
     * copy constructor
     */
    public Triangle3d(Triangle3d t) {
        a = new Vect3d(t.a);
        b = new Vect3d(t.b);
        c = new Vect3d(t.c);
    }

    /**
     * constructor given three points
     */
    public Triangle3d(Vect3d va, Vect3d vb, Vect3d vc) {
        a = new Vect3d(va);
        b = new Vect3d(vb);
        c = new Vect3d(vc);
    }

    /**
     * set new values
     */
    public void set(Vect3d va, Vect3d vb, Vect3d vc) {
        a = va;
        b = vb;
        c = vc;
    }

    /**
     * get a
     */
    public Vect3d getA() {
        return a;
    }

    /**
     * get b
     */
    public Vect3d getB() {
        return b;
    }

    /**
     * get c
     */
    public Vect3d getC() {
        return c;
    }

    /**
     * get the vertex i
     */
    public Vect3d getPoint(int i) {
        return (i == 0 ? a : (i == 1 ? b : c));
    }

    /**
     * get the vertex the set of vertices
     */
    public Vect3d[] getPoints() {
        Vect3d[] vt = {a, b, c};
        return vt;
    }

    /**
     * get a copy
     */
    public Triangle3d copy() {
        return new Triangle3d(a, b, c);
    }

    /**
     * set new value to a
     */
    public void setA(Vect3d pa) {
        a = pa;
    }

    /**
     * set new value to b
     */
    public void setB(Vect3d pb) {
        b = pb;
    }

    /**
     * set new value to c
     */
    public void setC(Vect3d pc) {
        c = pc;
    }

    /**
     * get the normal vector
     */
    public Vect3d normal() {
        Vect3d v1 = new Vect3d(b.sub(a));
        Vect3d v2 = new Vect3d(c.sub(a));
        Vect3d n = new Vect3d(v1.xProduct(v2));
        double longi = n.module();

        return (n.scalarMul(1.0 / longi));
    }

    /**
     * show the vertices
     */
    public void out() {
        System.out.println("Triangle3d: (" + a + "-" + b + "-" + c + ")");
    }

    public double area() {
        Vect3d AB = b.sub(a);
        Vect3d AC = c.sub(a);

        return (AB.module() * AC.module()) / 2;
    }

    public PointPosition classify(Vect3d p) {
        Vect3d v = new Vect3d(p.sub(a));
        double len = v.module();

        if (BasicGeom.equal(len, 0)) {
            return PointPosition.COPLANAR;
        }

        v = v.scalarMul(1 / len);

        double d = v.dot(normal());
        if (d > BasicGeom.ZERO) {
            return PointPosition.POSITIVE;
        } else if (d < -BasicGeom.ZERO) {
            return PointPosition.NEGATIVE;
        } else {
            return PointPosition.COPLANAR;
        }
    }

    public boolean ray_tri(Ray3d r, Vect3d p) {
        Vect3d ray_origin = r.orig;
        Vect3d ray_vector = r.dest.sub(r.orig);

        Vect3d vertex_0 = this.a;
        Vect3d vertex_1 = this.b;
        Vect3d vertex_2 = this.c;

        Vect3d edge_1 = vertex_1.sub(vertex_0);
        Vect3d edge_2 = vertex_2.sub(vertex_0);

        Vect3d h = ray_vector.xProduct(edge_2);
        Vect3d s = ray_origin.sub(vertex_0);
        Vect3d q = s.xProduct(edge_1);

        double a, f, u, v;
        a = edge_1.dot(h);

        if (a > -BasicGeom.ZERO && a < BasicGeom.ZERO) {
            return false; // this ray is parallel to this triangle
        }
        f = 1.0f / a;

        u = f * (s.dot(h));
        if (u < 0.0f || u > 1.0f) {
            return false;
        }

        v = f * ray_vector.dot(q);
        if (v < 0.0f || u + v > 1.0f) {
            return false;
        }

        // At this stage we can compute t to find out where intersection point is on the line
        double t = f * edge_2.dot(q);
        if (t > BasicGeom.ZERO) {
            Vect3d point = r.getPoint(t);
            p.setVert(point.x, point.y, point.z);
            return true;
        } else {
            return false;
        }
    }

    public boolean tri_plane(Plane pl, RelationTriPlane intersection) {
        double dot_1, dot_2, dot_3;
        Vect3d point_on_plane = pl.getPointParametric(0, 0);

        dot_1 = pl.getNormal().dot(a.sub(point_on_plane));
        dot_2 = pl.getNormal().dot(b.sub(point_on_plane));
        dot_3 = pl.getNormal().dot(c.sub(point_on_plane));

        if (Math.abs(dot_1) <= BasicGeom.ZERO) {
            dot_1 = 0.0f;
        }
        if (Math.abs(dot_2) <= BasicGeom.ZERO) {
            dot_2 = 0.0f;
        }
        if (Math.abs(dot_3) <= BasicGeom.ZERO) {
            dot_3 = 0.0f;
        }

        if (dot_1 > 0 && dot_2 > 0 && dot_3 > 0) {
            intersection.type = TypeRelationTriPlane.POSITIVE;
            return false;
        }
        if (dot_1 < 0 && dot_2 < 0 && dot_3 < 0) {
            intersection.type = TypeRelationTriPlane.NEGATIVE;
            return false;
        }

        if (Math.abs(dot_1) + Math.abs(dot_2) + Math.abs(dot_3) == 0) // Coplanar Case
        {
            intersection.type = TypeRelationTriPlane.COPLANAR;
            return true;
        }

        // Most Common Intersection
        if ((dot_1 > 0 && dot_2 > 0 && dot_3 < 0)
                || (dot_1 < 0 && dot_2 < 0 && dot_3 > 0)) {
            intersection.type = TypeRelationTriPlane.SEGMENT;
            Line3d l1 = new Line3d(a, c);
            Line3d l2 = new Line3d(b, c);

            Vect3d p1, p2;

            p1 = pl.intersect(l1);
            p2 = pl.intersect(l2);

            intersection.s = new Segment3d(p1, p2);
            return true;
        }

        if ((dot_2 > 0 && dot_3 > 0 && dot_1 < 0)
                || (dot_2 < 0 && dot_3 < 0 && dot_1 > 0)) {
            intersection.type = TypeRelationTriPlane.SEGMENT;
            Line3d l1 = new Line3d(b, a);
            Line3d l2 = new Line3d(c, a);

            Vect3d p1, p2;

            p1 = pl.intersect(l1);
            p2 = pl.intersect(l2);

            intersection.s = new Segment3d(p1, p2);
            return true;
        }

        if ((dot_1 > 0 && dot_3 > 0 && dot_2 < 0)
                || (dot_1 < 0 && dot_3 < 0 && dot_2 > 0)) {
            intersection.type = TypeRelationTriPlane.SEGMENT;
            Line3d l1 = new Line3d(a, b);
            Line3d l2 = new Line3d(c, b);

            Vect3d p1, p2;

            p1 = pl.intersect(l1);
            p2 = pl.intersect(l2);

            intersection.s = new Segment3d(p1, p2);
            return true;
        }

        if (dot_1 == 0 && ((dot_2 > 0 && dot_3 > 0) || (dot_2 < 0 && dot_3 < 0))) {
            intersection.type = TypeRelationTriPlane.POINT;
            intersection.p = a;
            return true;
        }

        if (dot_2 == 0 && ((dot_1 > 0 && dot_3 > 0) || (dot_1 < 0 && dot_3 < 0))) {
            intersection.type = TypeRelationTriPlane.POINT;
            intersection.p = b;
            return true;
        }

        if (dot_3 == 0 && ((dot_2 > 0 && dot_1 > 0) || (dot_2 < 0 && dot_1 < 0))) {
            intersection.type = TypeRelationTriPlane.POINT;
            intersection.p = c;
            return true;
        }

        if (dot_1 == 0 && ((dot_2 > 0 && dot_3 < 0) || (dot_2 < 0 && dot_3 > 0))) {
            intersection.type = TypeRelationTriPlane.SEGMENT;
            Line3d l1 = new Line3d(c, b);

            Vect3d p1;

            p1 = pl.intersect(l1);

            intersection.s = new Segment3d(a, p1);
            return true;
        }

        if (dot_2 == 0 && ((dot_1 > 0 && dot_3 < 0) || (dot_1 < 0 && dot_3 > 0))) {
            intersection.type = TypeRelationTriPlane.SEGMENT;
            Line3d l1 = new Line3d(a, c);

            Vect3d p1;

            p1 = pl.intersect(l1);

            intersection.s = new Segment3d(b, p1);
            return true;
        }

        if (dot_2 == 0 && ((dot_1 > 0 && dot_3 < 0) || (dot_1 < 0 && dot_3 > 0))) {
            intersection.type = TypeRelationTriPlane.SEGMENT;
            Line3d l1 = new Line3d(a, b);

            Vect3d p1;

            p1 = pl.intersect(l1);

            intersection.s = new Segment3d(c, p1);
            return true;
        }

        if (dot_1 == 0 && dot_2 == 0) {
            intersection.type = TypeRelationTriPlane.SEGMENT;

            intersection.s = new Segment3d(a, b);
            return true;
        }

        if (dot_2 == 0 && dot_3 == 0) {
            intersection.type = TypeRelationTriPlane.SEGMENT;

            intersection.s = new Segment3d(b, c);
            return true;
        }

        if (dot_1 == 0 && dot_3 == 0) {
            intersection.type = TypeRelationTriPlane.SEGMENT;

            intersection.s = new Segment3d(a, c);
            return true;
        }

        return false;
    }

    public boolean tri_AABB(AABB box) {
        return MollerTriAABB.triBoxOverlap(box.getCenter(), box.getExtent(), this);
    }

    public double angleToPoint(Vect3d p) {
        Triangle3d tri = new Triangle3d(a, b, p);
        Vect3d n_p = tri.normal();
        Vect3d n = normal();
        
        n = n.scalarMul(1/n.module());
        n_p = n_p.scalarMul(n_p.module());
        
        
        return Math.toDegrees(Math.acos(n_p.dot(n)));
        
    }
}
