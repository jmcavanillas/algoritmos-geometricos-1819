/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib3D;

import java.util.ArrayList;
import javax.media.opengl.GL;

/**
 *
 * @author Alberto
 */
public class Grid3d {
// Atributos

    private Casilla3D[][][] malla;
    private int divisiones;
    private Vect3d min, max;

//Constructores
    public Grid3d(Vect3d min, Vect3d max, int divisiones) {
        this.divisiones = divisiones;
        this.min = min;
        this.max = max;
        malla = new Casilla3D[divisiones + 1][divisiones + 1][divisiones + 1];

        float modX = (float) ((max.x - min.x) / (divisiones + 1));
        float modY = (float) ((max.y - min.y) / (divisiones + 1));
        float modZ = (float) ((max.z - min.z) / (divisiones + 1));

        // i y
        // j x
        // k z
        Vect3d prev = new Vect3d();
        Vect3d next = new Vect3d();

        for (int i = divisiones; i >= 0; --i) {
            prev.y = min.y + modY * (divisiones - i);
            next.y = min.y + modY * (divisiones - i + 1);
            for (int j = 0; j <= divisiones; ++j) {
                prev.x = min.x + modX * j;
                next.x = min.x + modX * (j + 1);
                for (int k = divisiones; k >= 0; --k) {
                    prev.z = min.z + modZ * (divisiones - k);
                    next.z = min.z + modZ * (divisiones - k + 1);
                    malla[i][j][k] = new Casilla3D(new DrawAABB(new AABB(prev, next)), prev, next);
                    prev = new Vect3d(prev);
                    next = new Vect3d(next);
                }
            }
        }
    }

//Metodos
    public Casilla3D getCasilla(Vect3d v) {
        float modX = (float) ((max.x - min.x) / (divisiones + 1));
        float modY = (float) ((max.y - min.y) / (divisiones + 1));
        float modZ = (float) ((max.z - min.z) / (divisiones + 1));

        int y = ((int) Math.abs((v.y - min.y) / modY));
        int x = ((int) Math.abs((v.x - min.x) / modX));
        int z = ((int) Math.abs((v.z - min.z) / modZ));
        y = divisiones - y;
        z = divisiones - z;

        y = Math.min(y, divisiones);
        x = Math.min(x, divisiones);
        z = Math.min(z, divisiones);

        y = Math.max(y, 0);
        x = Math.max(x, 0);
        z = Math.max(z, 0);
        Casilla3D ca = malla[y][x][z];

        return malla[y][x][z];
    }

    public void insertar(Particula p) {
        this.getCasilla(p.getPosicion()).insertar(p);
    }

    public void borrar(Particula p) {
        this.getCasilla(p.getPosicion()).borrar(p);
    }

    public boolean contiene(Particula p) {
        return this.getCasilla(p.getPosicion()).contiene(p);
    }

    public void calcularFuerzas(int nivel) {
        for (int i = divisiones; i >= 0; --i) {
            for (int j = 0; j <= divisiones; ++j) {
                for (int k = divisiones; k >= 0; --k) {
                    Casilla3D cas = malla[i][j][k];
                    ArrayList<Particula> particulas = cas.getParticulas();
                    for (int c = 0; c < particulas.size(); c++) {
                        Particula p = particulas.get(c);
                        fuerzaParticulaCasilla(i, j, k, p, nivel);
                    }
                }
            }
        }
    }

    public void fuerzaParticulaCasilla(int i, int j, int k, Particula p, int nivel) {
        for (int y = i - nivel; y <= i + nivel; ++y) {
            if (y >= 0 && y <= divisiones) {
                for (int x = j - nivel; x <= j + nivel; ++x) {
                    if (x >= 0 && x <= divisiones) {
                        for (int z = k - nivel; z <= k + nivel; ++z) {
                            if (z >= 0 && z <= divisiones) {
                                ArrayList<Particula> particulasO = malla[y][x][z].getParticulas();
                                for (int m = 0; m < particulasO.size(); m++) {
                                    Particula o = particulasO.get(m);
                                    if (!p.equals(o)) {
                                        if (p.unirse(o)) {
                                            particulasO.remove(o);
                                            m--;
                                        } else {
                                            p.calcularFuerza(o);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void moverParticulas() {
        ArrayList<Particula> salientes = new ArrayList<Particula>();
        for (int i = divisiones; i >= 0; --i) {
            for (int j = 0; j <= divisiones; ++j) {
                for (int k = divisiones; k >= 0; --k) {
                    Casilla3D c = malla[i][j][k];
                    ArrayList<Particula> particulas = c.getParticulas();
                    for (int l = 0; l < particulas.size(); l++) {
                        Particula p = particulas.get(l);
                        p.desplazarse();
                        if (!c.particulaInAABB(p)) {
                            malla[i][j][k].borrar(p);
                            salientes.add(p);
                            l--;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < salientes.size(); i++) {
            Particula get = salientes.get(i);
            Vect3d v = get.getPosicion();

            if (v.y < min.y || v.y > max.y) {
                get.direccion.y = -get.direccion.y;
            }
            if (v.x < min.x || v.x > max.x) {
                get.direccion.x = -get.direccion.x;
            }
            if (v.z < min.z || v.z > max.z) {
                get.direccion.z = -get.direccion.z;
            }
            this.insertar(get);
        }
    }

    public void dibujar(GL g) {
        for (int i = 0; i <= divisiones; ++i) {
            for (int j = 0; j <= divisiones; ++j) {
                for (int k = 0; k <= divisiones; ++k) {
                    Casilla3D c = malla[i][j][k];
                    malla[i][j][k].dibujar(g);
                }
            }
        }
    }

//Clases internas
    public class Casilla3D {
        // Atributos

        ArrayList<Particula> particulas;
        DrawAABB caja;
        Vect3d min, max;

        // Contructores 
        public Casilla3D(DrawAABB caja) {
            this.caja = caja;
        }

        public Casilla3D(DrawAABB caja, Vect3d min, Vect3d max) {
            particulas = new ArrayList<Particula>();
            this.caja = caja;
            this.min = min;
            this.max = max;
        }

        //Metodos
        public void insertar(Particula p) {
            particulas.add(p);
        }

        public void borrar(Particula p) {
            particulas.remove(p);
        }
        
        public boolean particulaInAABB(Particula p) {
            return caja.box.pointAABB(p.getPosicion());
        }
        
        public boolean contiene(Particula p) {
            return particulas.contains(p);
        }


        public void dibujar(GL g) {
            for (int i = 0; i < particulas.size(); i++) {
                Particula get = particulas.get(i);
                get.dibujar(g);
            }
            if (!Test.ocultar) {
                caja.drawObjectC(g, 1, 1, 1, 0.5f);
            }
        }

        //Getters y Setters
        public ArrayList<Particula> getParticulas() {
            return particulas;
        }

    }
}
