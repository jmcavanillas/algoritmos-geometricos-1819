package algGeom.lib3D;

import Util.*;
import algGeom.lib3D.Face.RelationTriPlane;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public final class TriangleMesh {

    ArrayList<Face> faces;
    ArrayList<Vect3d> vertices;
    boolean setNormal;

    Octree oc;

    public Octree getOc() {
        return oc;
    }

    public void setOc(Octree oc) {
        this.oc = oc;
    }

    /**
     * constructor from an obj file
     */
    public TriangleMesh(String filename) throws IOException {
        faces = new ArrayList<Face>();
        vertices = new ArrayList<Vect3d>();
        setNormal = false;
        String obj = "obj";

        File file;
        FileReader fr;
        BufferedReader reader;

        file = new File(filename);
        fr = new FileReader(file);
        reader = new BufferedReader(fr);

        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
        if (!extension.equals(obj)) {
            System.out.println("Only obj model sare supported ");
        } else {
            loadobject(reader);
            setNormals();
            setNormal = true;
        }
    }
    
    public TriangleMesh(List<Triangle3d> triangles)
    {
        faces = new ArrayList<Face>();
        vertices = new ArrayList<Vect3d>();
        for(Triangle3d t : triangles)
        {
            if (!vertices.contains(t.getA())) vertices.add(t.getA());
            if (!vertices.contains(t.getB())) vertices.add(t.getB());
            if (!vertices.contains(t.getC())) vertices.add(t.getC());
        }
        
        for (Triangle3d t : triangles)
        {
            
            faces.add(new Face(vertices.indexOf(t.getA())+1, 
                               vertices.indexOf(t.getB())+1, 
                               vertices.indexOf(t.getC())+1,
                               t.normal()));
        }
    }

    /**
     * get an array of triangles
     */
    public ArrayList<Triangle3d> getTriangules() {
        ArrayList<Triangle3d> triangles = new ArrayList<Triangle3d>();
        for (int i = 0; i < faces.size(); i++) {
            Face f = faces.get(i);
            Vect3d a = vertices.get(f.v1 - 1);
            Vect3d b = vertices.get(f.v2 - 1);
            Vect3d c = vertices.get(f.v3 - 1);
            triangles.add(new Triangle3d(a, b, c));
        }
        return triangles;
    }

    /**
     * constructor from an obj file
     */
    public double[] getVerticesTriangles() {
        double[] tri = new double[faces.size() * 9];
        for (int i = 0; i < faces.size(); i++) {
            Face f = faces.get(i);
            Vect3d a = vertices.get(f.v1 - 1);
            tri[i * 9] = a.x;
            tri[i * 9 + 1] = a.y;
            tri[i * 9 + 2] = a.z;
            Vect3d b = vertices.get(f.v2 - 1);
            tri[i * 9 + 3] = b.x;
            tri[i * 9 + 4] = b.y;
            tri[i * 9 + 5] = b.z;
            Vect3d c = vertices.get(f.v3 - 1);
            tri[i * 9 + 6] = c.x;
            tri[i * 9 + 7] = c.y;
            tri[i * 9 + 8] = c.z;
        }
        return tri;
    }

    /**
     * get all vertices
     */
    public double[] getVertices() {
        double[] vertex = new double[3 * vertices.size()];
        int j = 0;
        for (int i = 0; i < vertices.size(); i++) {
            vertex[j++] = vertices.get(i).x;
            vertex[j++] = vertices.get(i).y;
            vertex[j++] = vertices.get(i).z;

        }
        return vertex;
    }

    /**
     * get the index of all faces
     */
    public int[] getIndiceFaces() {
        int[] faces = new int[3 * this.faces.size()];
        int j = 0;
        for (int i = 0; i < this.faces.size(); i++) {
            faces[j++] = this.faces.get(i).v1;
            faces[j++] = this.faces.get(i).v2;
            faces[j++] = this.faces.get(i).v3;
        }
        return faces;
    }

    /**
     * get triangle in position i
     */
    public Triangle3d getTriangle(int i) {
        Face f = faces.get(i);
        Vect3d a = vertices.get(f.v1 - 1);
        Vect3d b = vertices.get(f.v2 - 1);
        Vect3d c = vertices.get(f.v3 - 1);
        return new Triangle3d(a, b, c);
    }

    /**
     * get the vertex in position i
     */
    public Vect3d getVertice(int i) {
        return new Vect3d(vertices.get(i).getX(), vertices.get(i).getY(), vertices.get(i).getZ());
    }

    /**
     * define all the normals, assuming the correct orientation of triangles
     */
    public void setNormals() {
        for (int i = 0; i < faces.size(); i++) {
            Face f = faces.get(i);
            Vect3d a = vertices.get(f.v1 - 1);
            Vect3d b = vertices.get(f.v2 - 1);
            Vect3d c = vertices.get(f.v3 - 1);
            Triangle3d t = new Triangle3d(a, b, c);
            f.setNormal(t.normal());
        }
        setNormal = true;
    }

    /**
     * get array with all normal vectors
     */
    public double[] getNormals() {
        double[] nor = new double[3 * faces.size()];
        if (!setNormal) {
            setNormals();
        }
        for (int i = 0; i < faces.size(); i++) {
            Face f = faces.get(i);
            Vect3d b = f.getNormal();
            nor[i * 3] = b.x;
            nor[i * 3 + 1] = b.y;
            nor[i * 3 + 2] = b.z;
        }
        return nor;
    }

    /**
     * reader of obj model
     *
     * @param br variable fichero
     */
    public void loadobject(BufferedReader br) {
        String line = "";

        try {
            while ((line = br.readLine()) != null) {
                line = line.trim();
                line = line.replaceAll("  ", " ");
                if (line.length() > 0) {
                    if (line.startsWith("v ")) {
                        float[] vert = read3Floats(line);
                        Vect3d point = new Vect3d(vert[0], vert[1], vert[2]);
                        vertices.add(point);
                    } else if (line.startsWith("vt")) {

                        continue;

                    } else if (line.startsWith("vn")) {

                        continue;
                    } else if (line.startsWith("f ")) {
                        int[] faces = read3Integer(line);
                        this.faces.add(new Face(faces));
                    } else if (line.startsWith("g ")) {
                        continue;
                    } else if (line.startsWith("usemtl")) {
                        continue;
                    } else if (line.startsWith("mtllib")) {
                        continue;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("GL_OBJ_Reader.loadObject() failed at line: " + line);
        }

        System.out.println("Obj loader: vertices " + vertices.size()
                + " faces " + faces.size());

    }

    public int getFacesSize() {
        return faces.size();
    }

    public int getVerticesSize() {
        return vertices.size();
    }

    private int[] read3Integer(String line) {
        try {
            StringTokenizer st = new StringTokenizer(line, " ");
            st.nextToken();
            if (st.countTokens() == 2) {
                return new int[]{Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), 0};
            } else {
                return new int[]{Integer.parseInt(st.nextToken()),
                    Integer.parseInt(st.nextToken()),
                    Integer.parseInt(st.nextToken())};
            }
        } catch (NumberFormatException e) {
            System.out.println("GL_OBJ_Reader.read3Floats(): error on line '" + line + "', " + e);
            return null;
        }
    }

    private float[] read3Floats(String line) {
        try {
            StringTokenizer st = new StringTokenizer(line, " ");
            st.nextToken();
            if (st.countTokens() == 2) {
                return new float[]{Float.parseFloat(st.nextToken()),
                    Float.parseFloat(st.nextToken()),
                    0};
            } else {
                return new float[]{Float.parseFloat(st.nextToken()),
                    Float.parseFloat(st.nextToken()),
                    Float.parseFloat(st.nextToken())};
            }
        } catch (NumberFormatException e) {
            System.out.println("GL_OBJ_Reader.read3Floats(): error on line '" + line + "', " + e);
            return null;
        }
    }

    /**
     * get the AABB of the model
     */
    public AABB getAABB() {
        if (vertices.isEmpty()) {
            return new AABB(new Vect3d(0, 0, 0), new Vect3d(0, 0, 0));
        }

        Vect3d min = new Vect3d(vertices.get(0));
        Vect3d max = new Vect3d(vertices.get(0));

        for (Vect3d point : vertices) {
            if (min.x > point.x) {
                min.x = point.x;
            }
            if (min.y > point.y) {
                min.y = point.y;
            }
            if (min.z > point.z) {
                min.z = point.z;
            }

            if (max.x < point.x) {
                max.x = point.x;
            }
            if (max.y < point.y) {
                max.y = point.y;
            }
            if (max.z < point.z) {
                max.z = point.z;
            }
        }

        return new AABB(min, max);
    }

    public boolean rayTraversalExh(Ray3d r, Vect3d p, Triangle3d t) {

        ArrayList<Triangle3d> triangles = getTriangules();
        Vect3d pPrim = new Vect3d();
        double dist = Double.MAX_VALUE;

        for (int i = 0; i < triangles.size(); i++) {
            Triangle3d get = triangles.get(i);

            if (get.ray_tri(r, pPrim)) {
                if (r.orig.distance(pPrim) < dist) {
                    t.set(get.getA(), get.getB(), get.getC());
                    p.setVert(p.getX(), p.getY(), p.getZ());
                }
            }
        }
        if (dist < Double.MAX_VALUE) {
            return true;
        }
        return false;
    }

    public boolean rayTraversalExh(Ray3d r, ArrayList<Vect3d> p, ArrayList<Triangle3d> at) {

        ArrayList<Triangle3d> triangles = getTriangules();

        p.clear();
        at.clear();
        for (int i = 0; i < triangles.size(); i++) {
            Triangle3d get = triangles.get(i);
            Vect3d pPrim = new Vect3d();

            if (get.ray_tri(r, pPrim)) {
                p.add(pPrim);
                at.add(get);
            }
        }

        if (!p.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean pointIntoMesh(Vect3d p) {

        Ray3d r1 = new Ray3d(p, new Vect3d(0, 1, 0));
        Ray3d r2 = new Ray3d(p, new Vect3d(1, 1, 0));
        Ray3d r3 = new Ray3d(p, new Vect3d(0, 1, 1));

        ArrayList<Vect3d> ap = new ArrayList<Vect3d>();
        ArrayList<Triangle3d> at = new ArrayList<Triangle3d>();

        int size1 = 0, size2 = 0, size3 = 0;

        rayTraversalExh(r1, ap, at);
        if (rayTraversalExh(r1, ap, at)) {
            size1 = at.size();
            if (rayTraversalExh(r2, ap, at)) {
                size2 = at.size();

                if (size1 % 2 == size2) {
                    return size1 % 2 == 1;
                } else {
                    rayTraversalExh(r2, ap, at);
                    return size3 % 2 == 1;
                }
            }
        }
        return false;
    }

    public void classify(Plane pl) {

        for (int i = 0; i < faces.size(); i++) {
            Face f = faces.get(i);
            Vect3d a = vertices.get(f.v1 - 1);
            Vect3d b = vertices.get(f.v2 - 1);
            Vect3d c = vertices.get(f.v3 - 1);
            Triangle3d t = new Triangle3d(a, b, c);
            Face.RelationTriPlane r = new RelationTriPlane();
            t.tri_plane(pl, r);
            f.setRelationType(r);
        }

    }

    public boolean pointIntoMeshOct(Vect3d p) {
        OctreeNode n = oc.getPoint(p);
        if (n != null) {
            if (n.color == NodeColor.GREY) {
                return pointIntoMesh(p);
            }
            if (n.color == NodeColor.BLACK) {
                return true;
            }
        }
        return false;
    }
}
