/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib3D;

import Util.BasicGeom;
import java.util.Random;
import javax.media.opengl.GL;

/**
 *
 * @author Alberto
 */
public class Particula {
// Constantes
    static final double gConst = 0.0000000000667408;    // Constante de Grabitacion universar
    static final double tConst = 8640000;     // Constante para acelerar simulacion, 1 frame = 1 86400 
    static final double dConst = 12;        // Constante para limitar radio de fusion de particulas
    static final double rConst = 85;        // Constante para limitar radio de representacion de particulas
// Atributos
    private float masa;         // Masa de la particula --> determina su radio (Simplificacion)
    private Vect3d posicion;    // Posicion de la particula en instante t
    Vect3d direccion;           // Direccion de la particula en instante t
    private float R, G, B;

// Contructores 
    public Particula(float masa, Vect3d posicion) {
        this.masa = masa;
        this.posicion = posicion;
        this.direccion = new Vect3d();

        Random r = new Random();
        R = r.nextFloat();
        G = r.nextFloat();
        B = r.nextFloat();
    }

//Metodos
    public boolean calcularFuerza(Particula otro) {
        Vect3d r = this.posicion.scalarMul(-1).add(otro.getPosicion()); //Vector direccion 

        if (r.module() > BasicGeom.ZERO) {
            Vect3d f = r.scalarMul((gConst * this.masa * otro.getMasa()) / r.module() / r.module()); // Vector fuerza
            f = f.scalarMul(1 / masa);
            f = f.scalarMul(tConst);
            direccion = direccion.add(f);
        }
        return false;
    }

    public void desplazarse() {
        posicion = posicion.add(direccion);
    }

    public boolean unirse(Particula otro) {
        Vect3d r = this.posicion.scalarMul(-1).add(otro.getPosicion()); //Vector direccion 

        if (r.module() < Math.min(masa / 10, dConst)) {
            this.direccion = direccion.scalarMul(masa).add(otro.getDireccion().scalarMul(otro.getMasa())); //unimos las particulas y conservamos el momento
            this.masa += otro.getMasa();
            this.direccion = direccion.scalarMul(1 / this.masa);
            return true;
        }
        return false;
    }

    public void dibujar(GL g) {
        g.glColor3f(R * 0.2f * masa, G * 0.f * masa, B * 0.2f * masa);
        g.glPointSize((float) Math.min(masa, rConst));
        g.glBegin(GL.GL_POINTS);
        g.glVertex3d(posicion.x, posicion.y, posicion.z);
        g.glEnd();
    }

//Geters - Seters
    public float getMasa() {
        return masa;
    }

    public void setMasa(float masa) {
        this.masa = masa;
    }

    public Vect3d getPosicion() {
        return posicion;
    }

    public void setPosicion(Vect3d posicion) {
        this.posicion = posicion;
    }

    public Vect3d getDireccion() {
        return direccion;
    }

    public void setDireccion(Vect3d direccion) {
        this.direccion = direccion;
    }

    public float getR() {
        return R;
    }

    public void setR(float R) {
        this.R = R;
    }

    public float getG() {
        return G;
    }

    public void setG(float G) {
        this.G = G;
    }

    public float getB() {
        return B;
    }

    public void setB(float B) {
        this.B = B;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Particula other = (Particula) obj;
        if (Float.floatToIntBits(this.masa) != Float.floatToIntBits(other.masa)) {
            return false;
        }
        if (this.posicion != other.posicion && (this.posicion == null || !this.posicion.equals(other.posicion))) {
            return false;
        }
        if (this.direccion != other.direccion && (this.direccion == null || !this.direccion.equals(other.direccion))) {
            return false;
        }
        return true;
    }
// Destructor

}
