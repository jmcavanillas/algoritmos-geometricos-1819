package algGeom.lib3D;

import Util.*;
import algGeom.lib2D.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.StringTokenizer;

public class PointCloud3d {

    ArrayList<Vect3d> points;

    public PointCloud3d() {
        points = new ArrayList<Vect3d>();
    }

    public PointCloud3d(int size) {
        points = new ArrayList<Vect3d>();
        if (size > 0) {

            Random r = new Random(40);

            double x, y, z;
            int pos;
            for (int i = 0; i < size; i++) {
                pos = r.nextBoolean() ? 1 : -1;
                x = r.nextDouble() * pos * BasicGeom.RANGE;
                pos = r.nextBoolean() ? 1 : -1;
                y = r.nextDouble() * pos * BasicGeom.RANGE;
                pos = r.nextBoolean() ? 1 : -1;
                z = r.nextDouble() * pos * BasicGeom.RANGE;

                Vect3d p = new Vect3d(x, y, z);
                points.add(p);
            }
        }
    }

    public int size() {
        return points.size();
    }

    public void clear() {
        points.clear();
    }

    public PointCloud3d(String path) throws FileNotFoundException {
        points = new ArrayList<Vect3d>();

        String obj = "obj";

        File file;
        FileReader fr;
        BufferedReader reader;

        file = new File(path);
        fr = new FileReader(file);
        reader = new BufferedReader(fr);

        String extension = path.substring(path.lastIndexOf(".") + 1, path.length());
        if (!extension.equals(obj)) {
            System.out.println("Only obj model sare supported ");
        } else {
            String line = "";

            try {
                while ((line = reader.readLine()) != null) {
                    line = line.trim();
                    line = line.replaceAll("  ", " ");
                    if (line.length() > 0) {
                        if (line.startsWith("v ")) {
                            float[] vert = read3Floats(line);
                            Vect3d point = new Vect3d(vert[0], vert[1], vert[2]);
                            points.add(point);
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("GL_OBJ_Reader.loadObject() failed at line: " + line);
            }
        }
    }

    public void save(String nombre) throws SaveIOException, IOException {
        String info = "";
        for (Vect3d point : points) {
            info = info.concat(Double.toString(point.x)
                    + "/" + Double.toString(point.y)
                    + "/" + Double.toString(point.z));
            info += System.lineSeparator();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(nombre));
        writer.write(info);
        writer.close();
    }

    public Vect3d getPoint(int pos) {
        if ((pos >= 0) && (pos < points.size())) {
            return points.get(pos);
        }
        return null;
    }

    public void addPoint(Vect3d p) {
        points.add(p);
    }

    public ArrayList<Vect3d> getPoints() {
        return points;
    }

    AABB getAABB() {
        if (points.isEmpty()) {
            return new AABB(new Vect3d(0, 0, 0), new Vect3d(0, 0, 0));
        }

        Vect3d min = new Vect3d(points.get(0));
        Vect3d max = new Vect3d(points.get(0));

        for (Vect3d point : points) {
            if (min.x > point.x) {
                min.x = point.x;
            }
            if (min.y > point.y) {
                min.y = point.y;
            }
            if (min.z > point.z) {
                min.z = point.z;
            }

            if (max.x < point.x) {
                max.x = point.x;
            }
            if (max.y < point.y) {
                max.y = point.y;
            }
            if (max.z < point.z) {
                max.z = point.z;
            }
        }

        return new AABB(min, max);
    }

    private float[] read3Floats(String line) {
        try {
            StringTokenizer st = new StringTokenizer(line, " ");
            st.nextToken();
            if (st.countTokens() == 2) {
                return new float[]{Float.parseFloat(st.nextToken()),
                    Float.parseFloat(st.nextToken()),
                    0};
            } else {
                return new float[]{Float.parseFloat(st.nextToken()),
                    Float.parseFloat(st.nextToken()),
                    Float.parseFloat(st.nextToken())};
            }
        } catch (NumberFormatException e) {
            System.out.println("GL_OBJ_Reader.read3Floats(): error on line '" + line + "', " + e);
            return null;
        }
    }
}
