package algGeom.lib3D;

import Util.*;
import javax.media.opengl.GL;

public class DrawTriangleMesh extends Draw {

    TriangleMesh m;

    public DrawTriangleMesh(TriangleMesh m) {
        this.m = m;
    }

    @Override
    public void drawObject(GL g) {
        
        Triangle3d t = new Triangle3d(m.getTriangle(m.getFacesSize()-1));
            DrawTriangle3d dt = new DrawTriangle3d(t);
            dt.drawObjectC(g,1,0,0);
            
        for (int i = 0; i < m.getFacesSize() - 1 ; i++) {
             t = new Triangle3d(m.getTriangle(i));
             dt = new DrawTriangle3d(t);
            g.glColor4f(0,0,0,0.2f);
            dt.drawObject(g);
        }
    }
    
    public void drawObject(GL g, int index) {
        
        if(index >= m.getFacesSize()) index = m.getFacesSize();
        if(index < 1) index = 1;
        
        Triangle3d t = new Triangle3d(m.getTriangle(index -1));
            DrawTriangle3d dt = new DrawTriangle3d(t);
            dt.drawObjectC(g,1,0,0);
            
        for (int i = 0; i < index - 1 ; i++) {
             t = new Triangle3d(m.getTriangle(i));
             dt = new DrawTriangle3d(t);
            g.glColor4f(0,0,0,0.2f);
            dt.drawObject(g);
        }
    }

    public void drawObjectStrip(GL g) {

        for (int i = 0; i < m.getFacesSize(); i++) {
            Triangle3d t = new Triangle3d(m.getTriangle(i));
            DrawTriangle3d dt = new DrawTriangle3d(t);
            dt.drawObjectStrip(g);
        }

        g.glEnd();

    }

    public void drawObjectClassify(GL g) {

        for (int i = 0; i < m.getFacesSize(); i++) {
            Triangle3d t = new Triangle3d(m.getTriangle(i));
            DrawTriangle3d dt = new DrawTriangle3d(t);
            Face f = m.faces.get(i);
            if (f.relation.type == TypeRelationTriPlane.NEGATIVE) {
                g.glColor3f(0, 1, 0);
            }
            if (f.relation.type == TypeRelationTriPlane.POSITIVE) {
                g.glColor3f(0, 0, 1);
            }
            if (f.relation.type == TypeRelationTriPlane.NON) {
                g.glColor3f(1, 1, 0);
            }
            if (f.relation.type == TypeRelationTriPlane.SEGMENT) {
                g.glColor3f(1, 0, 0);
            }
            if (f.relation.type == TypeRelationTriPlane.COPLANAR) {
                g.glColor3f(0, 1, 1);
            }
            if (f.relation.type == TypeRelationTriPlane.POINT) {
                g.glColor3f(1, 0, 1);
            }
            dt.drawObject(g);
        }

        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glColor3f(R, G, B);
        drawObject(g);
    }
}
