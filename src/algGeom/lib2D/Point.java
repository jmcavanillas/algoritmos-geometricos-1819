package algGeom.lib2D;

import static java.lang.Math.*;

/**
 * Class that represents a 2D point
 */
enum PointClassification {

    LEFT, RIGHT, FORWARD, BACKWARD, BETWEEN, ORIGIN, DEST
};

public class Point {

    protected double x;
    protected double y;

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(double xx, double yy) {
        x = xx;
        y = yy;
    }

    public Point(Point p) {
        x = p.x;
        y = p.y;
    }

    /**
     * Constructor from an alpha angle (radians) and the vector module rp.If
     * polar == true, it must be builtup with the polar coordinates
     *
     * @param alpha
     * @param rp
     * @param polar
     */
    public Point(double alpha, double rp, boolean polar) {
        if (polar) {

            x = rp * cos(alpha);
            y = rp * sin(alpha);

        } else {
            x = alpha;
            y = rp;
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Return the alpha angle (radians) (note: consider the point quadrant)
     */
    public double getAlpha() {
        double angle;
        if (x != 0) {
            angle = atan(y / x);
            if (x < 0) {
                angle = Math.PI + angle;
            } else if (y > 0) {
                angle = 2 * Math.PI + angle;
            }
        } else {
            angle = Math.PI / 2;
            if (y < 0) {
                angle *= 3;
            }
        }

        return angle;
    }

    /**
     * Return the vector module rp (note: consider the point quadrant)
     *
     * @return
     */
    public double getModule() {
        return sqrt(x * x + y * y);
    }

    /**
     * Determines the relative position of a point (this) with respect to other
     * two given as a parameter (which can form a segment)
     *
     * @param p0
     * @param p1
     * @return
     */
    public PointClassification classify(Point p0, Point p1) {
        Point p2 = this;
        Point a = new Point(p1.x - p0.x, p1.y - p0.y);
        Point b = new Point(p2.x - p0.x, p2.y - p0.y);
        double sa = a.x * b.y - b.x * a.y;
        if (sa > 0.0) {
            return PointClassification.LEFT;
        }
        if (sa < 0.0) {
            return PointClassification.RIGHT;
        }
        if ((a.x * b.x < 0.0) || (a.y * b.y < 0.0)) {
            return PointClassification.BACKWARD;
        }
        if (a.getModule() < b.getModule()) {
            return PointClassification.FORWARD;
        }
        if (p0.distance(p2) < BasicGeom.ZERO) {
            return PointClassification.ORIGIN;
        }
        if (p1.distance(p2) < BasicGeom.ZERO) {
            return PointClassification.DEST;
        }
        return PointClassification.BETWEEN;
    }

    boolean equal(Point pt) {
        return (BasicGeom.equal(x, pt.x) && BasicGeom.equal(y, pt.y));

    }

    /**
     * Distance between two points
     *
     * @param p
     * @return
     */
    public double distance(Point p) {
        return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
    }

    /**
     * Calculate the double area of the triangle formed by (this, a, b)
     *
     * @param a
     * @param b
     * @return
     */
    public double triangleArea2(Point a, Point b) {
        return (this.x * a.y - this.y * a.x + a.x * b.y - a.y * b.x + b.x * this.y - b.y * this.x);
    }

    public boolean distinct(Point p) {
        return (Math.abs(x - p.x) > BasicGeom.ZERO || Math.abs(y - p.y) > BasicGeom.ZERO);
    }

    public Point copy() {
        return new Point(x, y);
    }

    public void copy(Point p) {
        x = p.x;
        y = p.y;
    }

    public Point get() {
        return this;
    }

    public void set(double xx, double yy) {
        x = xx;
        y = yy;
    }

    public void setX(double xx) {
        x = xx;
    }

    public void setY(double yy) {
        y = yy;
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean left(Point a, Point b) {
        return classify(a, b) == PointClassification.LEFT;
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean right(Point a, Point b) {
        return classify(a, b) == PointClassification.RIGHT;
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean colinear(Point a, Point b) {
        PointClassification resultado = classify(a, b);
        return (resultado != PointClassification.LEFT) && (resultado != PointClassification.RIGHT);
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean leftAbove(Point a, Point b) {
        PointClassification resultado = classify(a, b);
        return (resultado == PointClassification.LEFT) || (resultado != PointClassification.RIGHT);
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean rightAbove(Point a, Point b) {
        PointClassification resultado = classify(a, b);
        return (resultado == PointClassification.RIGHT) || (resultado != PointClassification.LEFT);
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean isBetween(Point a, Point b) {
        return classify(a, b) == PointClassification.BETWEEN;
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean forward(Point a, Point b) {
        return classify(a, b) == PointClassification.FORWARD;
    }

    /**
     * It must used the method classify
     *
     * @param a
     * @param b
     * @return
     */
    public boolean backward(Point a, Point b) {
        return classify(a, b) == PointClassification.BACKWARD;
    }

    /**
     * Calculate the slope between two points (this and a)
     *
     * @param p
     * @return
     */
    public double slope(Point p) {
        if (Math.abs(x - p.getX()) < BasicGeom.ZERO) {
            return (p.y - y) / (p.x - x);
        } else {
            return BasicGeom.INFINITY;
        }
    }

    /**
     * Muestra en pantalla los valores de las coordenadas del Point.
     */
    public void out() {
        System.out.print("Coordinate x: ");
        System.out.println(x);
        System.out.print("Coordinate y: ");
        System.out.println(y);
    }

}
