package algGeom.lib2D;

//import Util.BasicGeom;
//import java.util.ArrayList;
/**
 * This class can represent both a circle and a circle.
 */
public class Circle {

    public enum RelationCircles {

        CONCENTRIC,
        OUTER,
        INNER,
        SECANT,
        INNER_TANG,
        OUTER_TANG

    };

    public enum RelationCircleLine {

        INTERSECTS,
        TANGENTS,
        NO_INTERSECTS
    };

    static int NUMBER_SIDES = 30;

    protected Point c; // Circle center
    protected double r; // Circle radius

    public Circle() {
        c = new Point();
        r = 0;
    }

    public Circle(Point center, double radius) {
        c = center;
        r = radius;
    }

    public Circle(Circle cc) {
        c = cc.c;
        r = cc.r;
    }

    public Point getCenter() {
        return c;
    }

    public double getRadius() {
        return r;
    }

    public double area() {
        return BasicGeom.PI * r * r;
    }

    public double diameter() {
        return BasicGeom.PI * 2 * r;

    }

    /**
     * Returns true if the point is inside the circle
     */
    public boolean isInside(Point p) {
        if (p.distance(c) < r) {
            return true;
        }
        return false;
    }

    /**
     * Returns a polygon with the points that define the circle
     */
    public Polygon getPointsCircle() {
        Polygon c = new Polygon();

        int puntos = 100;
        double incremento = Math.PI * 2 / puntos;

        double sum = 0;
        for (int i = 0; i < puntos; i++) {

            c.add(new Point(this.c.x + Math.sin(sum) * r, this.c.y + Math.cos(sum) * r));
            sum += incremento;
        }
        return c;
    }

    /**
     * Returns the relation between two circles
     *
     * @param c
     * @return
     */
    public RelationCircles circleRelation(Circle c) {
        Vector O_1 = new Vector(this.c);
        Vector O_2 = new Vector(c.c);
        double distance = O_2.sub(O_1).getModule();
        double sum_r = this.r + c.r;
        double sub_r = c.r - this.r;

        if (BasicGeom.equal(distance, 0)) {
            return RelationCircles.CONCENTRIC;
        }

        if (distance > (sum_r + BasicGeom.ZERO)) {
            return RelationCircles.OUTER;
        }

        if (distance < (sub_r - BasicGeom.ZERO)) {
            return RelationCircles.INNER;
        }

        if (BasicGeom.equal(distance, sum_r)) {
            return RelationCircles.OUTER_TANG;
        }

        if (sub_r + BasicGeom.ZERO < distance
                && distance < sum_r - BasicGeom.ZERO) {
            return RelationCircles.SECANT;
        }

        return RelationCircles.INNER_TANG;
    }

    /**
     * Returns the relation between a circle and a line
     *
     * @param l
     * @return
     */
    public RelationCircleLine lineRelation(Line l) {
        Vector center = new Vector(this.c);
        double distance = l.distPointLine(center);

        if (distance < r - BasicGeom.ZERO) {
            return RelationCircleLine.INTERSECTS;
        }

        if (BasicGeom.equal(distance, r)) {
            return RelationCircleLine.TANGENTS;
        }

        return RelationCircleLine.NO_INTERSECTS;
    }

    /**
     * Check if the line intersects with the circle and returns the two
     * intersection points, if any
     */
    public RelationCircleLine intersects(Line l, Vector inner1, Vector inner2) {

        Vector delta = new Vector(l.a).sub(new Vector(c));
        Vector lineDir = new Vector(l.b).sub(new Vector(l.a));
        double gamma = Math.pow(lineDir.dot(delta), 2)
                - Math.pow(lineDir.getModule(), 2)
                * (Math.pow(delta.getModule(), 2) - Math.pow(r, 2));

        if (gamma < 0) {
            return RelationCircleLine.NO_INTERSECTS;
        }

        double t_1 = (-1 * (lineDir.dot(delta)) + Math.sqrt(gamma))
                / Math.pow(lineDir.getModule(), 2);
        double t_2 = (-1 * (lineDir.dot(delta)) - Math.sqrt(gamma))
                / Math.pow(lineDir.getModule(), 2);
        try {
            inner1.x = new Vector(l.getPoint(t_1)).x;
            inner1.y = new Vector(l.getPoint(t_1)).y;
            inner2.x = new Vector(l.getPoint(t_2)).x;
            inner2.y = new Vector(l.getPoint(t_2)).y;
        } catch (SegmentLine.Invalid_T_Parameter e) {
        }

        if (gamma == 0) {
            return RelationCircleLine.TANGENTS;
        }

        return RelationCircleLine.INTERSECTS;
    }

    /**
     * Check if the segment intersects with the circle and returns the two
     * intersection points, if any
     */
    public RelationCircleLine intersects(SegmentLine l, Vector inner1, Vector inner2) {
        Vector delta = new Vector(l.a).sub(new Vector(c));
        Vector lineDir = new Vector(l.b).sub(new Vector(l.a));
        double gamma = Math.pow(lineDir.dot(delta), 2)
                - Math.pow(lineDir.getModule(), 2)
                * (Math.pow(delta.getModule(), 2) - Math.pow(r, 2));

        if (gamma < 0) {
            return RelationCircleLine.NO_INTERSECTS;
        }

        double t_1 = (-1 * (lineDir.dot(delta)) + Math.sqrt(gamma))
                / Math.pow(lineDir.getModule(), 2);
        double t_2 = (-1 * (lineDir.dot(delta)) - Math.sqrt(gamma))
                / Math.pow(lineDir.getModule(), 2);

        if ((t_1 >= 0 && t_1 <= 1) || (t_2 >= 0 && t_2 <= 1)) {
            try {
                inner1.x = new Vector(l.getPoint(t_1)).x;
                inner1.y = new Vector(l.getPoint(t_1)).y;
                inner2.x = new Vector(l.getPoint(t_2)).x;
                inner2.y = new Vector(l.getPoint(t_2)).y;
            } catch (SegmentLine.Invalid_T_Parameter e) {
            }
            if (gamma == 0) {
                return RelationCircleLine.TANGENTS;
            }

            return RelationCircleLine.INTERSECTS;
        }

        return RelationCircleLine.NO_INTERSECTS;
    }

    /**
     * Check if the line intersects with a ray and returns the two intersection
     * points, if any
     */
    public RelationCircleLine intersects(RayLine l, Vector inner1, Vector inner2) {
        Vector delta = new Vector(l.a).sub(new Vector(c));
        Vector lineDir = new Vector(l.b).sub(new Vector(l.a));
        double gamma = Math.pow(lineDir.dot(delta), 2)
                - Math.pow(lineDir.getModule(), 2)
                * (Math.pow(delta.getModule(), 2) - Math.pow(r, 2));

        if (gamma < 0) {
            return RelationCircleLine.NO_INTERSECTS;
        }

        double t_1 = (-1 * (lineDir.dot(delta)) + Math.sqrt(gamma))
                / Math.pow(lineDir.getModule(), 2);
        double t_2 = (-1 * (lineDir.dot(delta)) - Math.sqrt(gamma))
                / Math.pow(lineDir.getModule(), 2);

        if (t_1 >= 0 || t_2 >= 0) {
            try {
                inner1.x = new Vector(l.getPoint(t_1)).x;
                inner1.y = new Vector(l.getPoint(t_1)).y;
                inner2.x = new Vector(l.getPoint(t_2)).x;
                inner2.y = new Vector(l.getPoint(t_2)).y;

            } catch (SegmentLine.Invalid_T_Parameter e) {
            }
            if (gamma == 0) {
                return RelationCircleLine.TANGENTS;
            }

            return RelationCircleLine.INTERSECTS;
        }

        return RelationCircleLine.NO_INTERSECTS;
    }

}
