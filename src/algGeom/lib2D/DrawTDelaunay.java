/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib2D;

import java.util.ArrayList;
import java.util.List;
import javax.media.opengl.GL;

/**
 *
 * @author Javier Mart�nez Cavanillas
 */
public class DrawTDelaunay extends Draw
{
    TDelaunay triangulation;
    
    public DrawTDelaunay()
    {
        triangulation = new TDelaunay(0);
    }
    
    public DrawTDelaunay(TDelaunay t) {
        triangulation = t;
    }

    @Override
    public void drawObject(GL gl)
    {
        for (SegmentLine l : triangulation.getEdges())
        {
            DrawSegment s = new DrawSegment(l);
            s.drawObject(gl);
        }
    }

    @Override
    public void drawObjectC(GL gl, float R, float G, float B)
    {
        for (SegmentLine l : triangulation.getEdges())
        {
            DrawSegment s = new DrawSegment(l);
            s.drawObjectC(gl, R, G, B);
        }
    }
    
    
}
