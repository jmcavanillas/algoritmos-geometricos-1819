package algGeom.lib2D;

import Util.MutableDouble;


/**
 * Class that represents a ray in the plane
 */
public class RayLine extends SegmentLine{
    
    public RayLine (Point a, Point b){
    	super (a,b);
    }
	
    public RayLine (SegmentLine s){
        a = s.a;
        b = s.b;
    }
    
    @Override
    public boolean segmentIntersection (SegmentLine l) {
        throw new UnsupportedOperationException();
    }
    @Override
    public boolean impSegmentIntersection (SegmentLine l) {
        throw new UnsupportedOperationException();
    }
 
    /**
     * t paramenter should be > 0
     */
    @Override
    protected void check_t(double t) throws Invalid_T_Parameter {
        if (t < 0) throw new Invalid_T_Parameter();
    }
    
    
    
    public boolean intersects(Line r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
            } catch (Invalid_T_Parameter e){}

            if( s.getValue() >= 0)
                return true;
        }
        return false;
    }
    
    public boolean intersects(RayLine r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
            } catch (Invalid_T_Parameter e){}
            
            if (s.getValue() >= 0 && t.getValue() >= 0)
                return true;
        }
        return false;
    }
    
    public boolean intersects(SegmentLine r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}
            
            if (s.getValue() >= 0 && t.getValue() >= 0 && t.getValue() <= 1)
                return true;
        }
        return false;
    }
 
    public double distPointRay(Vector A)
    {
        Vector P = new Vector(a);
        Vector d = new Vector(b).sub(P);
        
        Double t = d.dot(A.sub(P)) / d.dot(d);
        
        if (t <= 0)
            return A.sub(P).getModule();
        
        return A.sub(P.add(d.scalarMult(t))).getModule();
    }
    
    public void out () {
        System.out.println ("RayLine->");
        System.out.println ("Punto a: ");
        a.out ();
        System.out.println ("Punto b: ");
        b.out ();
    }
    
}
