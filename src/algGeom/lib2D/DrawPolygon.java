package algGeom.lib2D;

import javax.media.opengl.*;

/**
 * Class for drawing a Polygon
 */
public class DrawPolygon extends Draw {

    Polygon vp;

    public DrawPolygon(Polygon p) {
        vp = p;
    }

    /**
     * Draw the interior of the polygon
     */
    @Override
    public void drawObject(GL g) {

        int tama = vp.vertexSize();
        double x, y;
        g.glBegin(GL.GL_LINE_LOOP);
        for (int i = 0; i < tama; ++i) {
            x = vp.getVertexAt(i).x;
            y = vp.getVertexAt(i).y;
            x = convCoordX(x);
            y = convCoordX(y);
            g.glVertex2d(x, y);
        }

        g.glEnd();

    }

    public void drawObjectLine(GL g) {

        int tama = vp.vertexSize();
        double x, y;
        g.glBegin(GL.GL_LINE_LOOP);
        for (int i = 0; i < tama; ++i) {
            x = vp.getVertexAt(i).x;
            y = vp.getVertexAt(i).y;
            x = convCoordX(x);
            y = convCoordX(y);
            g.glVertex2d(x, y);
        }

        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {

        g.glColor3f(R, G, B);
        drawObject(g);

    }

}
