package algGeom.lib2D;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Class that represents a Polygon fromed by Vertex objects
 */
public class Polygon {

    protected int nVertexs;
    protected ArrayList<Vertex> Vertexs;

    /**
     * Default empty constructor
     */
    public Polygon() {
        Vertexs = new ArrayList<Vertex>();
        nVertexs = 0;
    }

    public Polygon(int nV) {
        Vertexs = new ArrayList<Vertex>(nV);
        nVertexs = nV;
    }

    public Polygon(Polygon pl) {
        Vertexs = new ArrayList<Vertex>(pl.Vertexs);
        nVertexs = pl.nVertexs;
    }

    public Polygon(ArrayList<Vertex> vert, int nV) {
        Vertexs = new ArrayList<Vertex>(vert);
        nVertexs = nV;
    }

    public Polygon copy() {
        Polygon nuevoPolygon = new Polygon(nVertexs);
        nuevoPolygon.Vertexs = new ArrayList<Vertex>(Vertexs);
        nuevoPolygon.nVertexs = nVertexs;
        return nuevoPolygon;
    }

    public void copy(Polygon pl) {
        Vertexs.clear();                           //Se limpia el vector.
        Vertexs = new ArrayList<Vertex>(pl.Vertexs);    //Se copy el vector.
        nVertexs = pl.nVertexs;
    }

    public void set(Vertex v, int pos) {
        if (pos >= 0 && pos < nVertexs) {
            Vertex antiguo = new Vertex((Vertex) Vertexs.get(pos));
            antiguo.setPolygon(null);
            antiguo.setPosition(-1);
            Vertexs.set(pos, (Vertex) v);
            v.setPolygon(this);
            v.setPosition(pos);
        }
    }

    /**
     * Adds the vertex in the last position
     *
     * @param v
     */
    public void add(Vertex v) {
        Vertexs.add((Vertex) v);
        v.setPolygon(this);
        v.setPosition(nVertexs);
        nVertexs++;
    }

    /**
     * Adds the point in the last position
     *
     * @param p
     */
    public void add(Point p) {
        Vertex v = new Vertex(p, this, nVertexs);
        Vertexs.add((Vertex) v);
        nVertexs++;
    }

    public Vertex getVertexAt(int pos) {
        if (pos >= 0 && pos < nVertexs) {
            return (Vertex) Vertexs.get(pos);
        } else {
            return null;
        }
    }

    public Vertex setVertexAt(int pos) {
        if (pos >= 0 && pos < nVertexs) {
            return new Vertex((Vertex) Vertexs.get(pos));
        } else {
            return null;
        }
    }

    public int vertexSize() {
        return nVertexs;
    }

    public SegmentLine getEdge(int i) {
        return new SegmentLine(getVertexAt(i), getVertexAt((i + 1) % nVertexs));
    }

    /**
     * Polygon builder from file
     *
     * @param nombre
     * @throws java.io.FileNotFoundException
     */
    public Polygon(String nombre) throws FileNotFoundException {
        Vertexs = new ArrayList<Vertex>();
        nVertexs = 0;
        File file = new File(nombre);
        Scanner sc = new Scanner(file);

        String[] coords;

        while (sc.hasNext()) {
            coords = sc.next().split("/");
            Vertex vertex = new Vertex(Double.parseDouble(coords[0]),
                    Double.parseDouble(coords[1]), this);
            vertex.setPosition(Vertexs.size());
            Vertexs.add(vertex);
            nVertexs++;
        }

    }

    /**
     * Saves the coordinates of the polygon in file with the same format as the
     * constructor
     *
     * @param nombre
     * @throws algGeom.lib2D.SaveIOException
     * @throws java.io.IOException
     */
    public void save(String nombre) throws SaveIOException, IOException {
        String info = "";
        for (Vertex vertex : Vertexs) {
            info = info.concat(Double.toString(vertex.x)
                    + "/" + Double.toString(vertex.y));
            info += System.lineSeparator();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(nombre));
        writer.write(info);
        writer.close();
    }

    /**
     * Assuming that this is a convex polygon, indicate if the point p is inside
     * the polygon
     *
     * @param pt
     * @return
     */
    public boolean pointInCovexPolygon(Point pt) {
        for (int i = 0; i < Vertexs.size() - 1; ++i) {
            if (!getEdge(i).left(pt)) {
                return false;
            }
        }

        return true;
    }

    public boolean convex() {
        for (Vertex vertex : Vertexs) {
            if (vertex.concave()) {
                return false;
            }
        }

        return true;
    }

    public boolean intersects(Line r, Vector interseccion) {
        for (int i = 0; i < Vertexs.size(); i++) {
            Vertex get = Vertexs.get(i);
            SegmentLine s = new SegmentLine(get.x, get.y, get.next().x, get.next().y);
            if(s.intersects(r, interseccion)){
                return true;
            }
        }
        return false;
    }

    public boolean intersects(RayLine r, Vector interseccion) {
        for (int i = 0; i < Vertexs.size(); i++) {
            Vertex get = Vertexs.get(i);
            SegmentLine s = new SegmentLine(get.x, get.y, get.next().x, get.next().y);
            if(s.intersects(r, interseccion)){
                return true;
            }
        }
        return false;
    }

    public boolean intersects(SegmentLine r, Vector interseccion) {
        for (int i = 0; i < Vertexs.size(); i++) {
            Vertex get = Vertexs.get(i);
            SegmentLine s = new SegmentLine(get.x, get.y, get.next().x, get.next().y);
            if(s.intersects(r, interseccion)){
                return true;
            }
        }
        return false;
    }

    public void out() {
        Vertex v;
        for (int i = 0; i < nVertexs; i++) {
            v = (Vertex) Vertexs.get(i);
            v.out();
        }
    }

}
