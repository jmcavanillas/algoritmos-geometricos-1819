package algGeom.lib2D;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.*;

/**
 * Class for drawing a ray
 */
public class DrawRay extends Draw {

    RayLine rl;

    public DrawRay(RayLine l) {
        rl = l;
    }

    public RayLine getRay() {
        return rl;
    }

    @Override
    public void drawObject(GL g) {
    
        // screen coordiantes
        double ax = 0, ay = 0, bx = 0, by = 0;
        double m = rl.slope();
        double c = rl.getC();

        Point p;
        try {
            p = rl.a;
            ax = p.x;
            ay = p.y;
            p = rl.getPoint(100);
            bx = p.x;
            by = p.y;
        } catch (SegmentLine.Invalid_T_Parameter ex) {
            Logger.getLogger(DrawLine.class.getName()).log(Level.SEVERE, null, ex);
        }

        ax = convCoordX(ax);
        ay = convCoordX(ay);
        bx = convCoordX(bx);
        by = convCoordX(by);

        g.glBegin(GL.GL_LINES);
        g.glVertex2d(ax, ay);
        g.glVertex2d(bx, by); //the fourth (w) component is zero!
        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glLineWidth(3.0f);
        g.glColor3f(R, G, B);
        drawObject(g);

    }

}
