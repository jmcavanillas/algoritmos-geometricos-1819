package algGeom.lib2D;

import Util.MutableDouble;

/**
 * Class that represents a segment line defined by two points
 */
public class SegmentLine
{

    protected Point a;
    protected Point b;

    static class Invalid_T_Parameter extends Exception
    {
    }

    protected void check_t(double t) throws Invalid_T_Parameter
    {
        if ((t < 0) || (t > 1))
        {
            throw new Invalid_T_Parameter();
        }
    }

    public SegmentLine()
    {
        b = new Point();
        a = new Point();
    }

    public SegmentLine(Point ii, Point ss)
    {
        a = ii;
        b = ss;
    }

    public SegmentLine(SegmentLine sg)
    {
        a = sg.a;
        b = sg.b;
    }

    public SegmentLine(double ax, double ay, double bx, double by)
    {
        a = new Point(ax, ay);
        b = new Point(bx, by);
    }

    /**
     * Returns the area formed by the triangle composed of the current
     * SegmentLine and the union of its bounds with p
     */
    public double triangleArea2(Point p)
    {
        return p.triangleArea2(a, b);
    }

    public double length()
    {
        return a.distance(b);
    }

    public boolean equal(SegmentLine sg)
    {
        return (a.equal(sg.a) && b.equal(sg.b)) || (a.equal(sg.b) && b.equal(sg.a));
    }

    public boolean distinct(SegmentLine sg)
    {
        return !(a.equal(sg.a) && b.equal(sg.b)) || (a.equal(sg.b) && b.equal(sg.a));
    }

    public SegmentLine copy()
    {
        return new SegmentLine(a, b);
    }

    public void copy(SegmentLine sg)
    {
        a.copy(sg.a);
        b.copy(sg.b);
    }

    public SegmentLine get()
    {
        return this;
    }

    public void setA(Point p)
    {
        a.copy(p);
    }

    /**
     * Determines whether a segment is horizontal or not (use BasicGeom.CERO)
     *
     * @return
     */
    public boolean isHorizontal()
    {
        return Math.abs(a.getY() - b.getY()) < BasicGeom.ZERO;
    }

    /**
     * Determines whether or not a segment is vertical (use BasicGeom.CERO)
     *
     * @return
     */
    public boolean isVertical()
    {
        return Math.abs(a.getX() - b.getX()) < BasicGeom.ZERO;
    }

    /**
     * Determines whether p is in the left of SegmentLine
     *
     * @param p
     * @return
     */
    public boolean left(Point p)
    {
        return p.left(a, b);
    }

    /**
     * It obtains the point belonging to the segment or colineal to it for a
     * concrete t in the parametric equation: result = a + t (b-a)
     */
    Point getPoint(double t) throws Invalid_T_Parameter
    {
        Vector dir = new Vector(b).sub(new Vector(a));
        dir = dir.scalarMult(t);

        return new Point(a.x + dir.x, a.y + dir.y);
    }

    public Point getA()
    {
        return a;
    }

    public Point getB()
    {
        return b;
    }

    /**
     * Returns the slope of the implied straight line equation: m = (yb-ya) /
     * (xb-xa)
     *
     * @return
     */
    public double slope()
    {
        if (isVertical())
        {
            return BasicGeom.INFINITY;
        }

        return (b.y - a.y) / (b.x - a.x);
    }

    /**
     * Returns the constant of the equation of the implied line: c = y-mx (use
     * BasicGeom.INFINITO)
     *
     * @return
     */
    public double getC()
    {
        double m = slope();
        if (m == BasicGeom.INFINITY)
        {
            return BasicGeom.INFINITY;
        }

        return a.y - m * a.x;
    }

    /**
     * Determines whether two segments intersect improperly, that is, when one
     * end of a segment is contained in the other. Use integer arithmetic.
     */
    public boolean impSegmentIntersection(SegmentLine l)
    {
        Point c = l.a;
        Point d = l.b;

        if (a.isBetween(b, c) || a.isBetween(b, d) || c.isBetween(d, a)
                || c.isBetween(d, b))
        {
            return true;
        } else
        {
            return (a.left(b, c) ^ a.left(b, d) ^ c.left(d, a) ^ d.left(d, b));
        }

    }

    /**
     * Determines whether two segments intersect in their own way, that is, when
     * they intersect completely.Use only arithmetic
     *
     * @param l
     * @return
     */
    public boolean segmentIntersection(SegmentLine l)
    {
        Point c = l.a;
        Point d = l.b;

        if (a.colinear(c, d) || b.colinear(c, d)
                || c.colinear(a, b) || d.colinear(a, b))
        {
            return false;
        } else
        {
            return a.left(c, d) ^ b.left(c, d)
                    && c.left(a, b) ^ d.left(a, b);
        }

    }

    protected boolean intersects(Vector C, Vector D, MutableDouble s, MutableDouble t)
    {
        Vector A = new Vector(a);
        Vector B = new Vector(b);
        
        Vector CD = D.sub(C);
        Vector AC = C.sub(A);
        Vector AB = B.sub(A);

        Double den = CD.x * AB.y - AB.x * CD.y;

        if (BasicGeom.equal(den, 0))
        {
            return false;
        }
        
        
        s.setValue((CD.x * AC.y - AC.x * CD.y) / den);
        t.setValue((AB.x * AC.y - AC.x * AB.y) / den);

        return true;
    }

    public boolean intesects(Line r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}
            
            if (s.getValue() <= 1 && s.getValue() >= 0)
                return true;
        }
        return false;
    }
    
    public boolean intersects(RayLine r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}
            
            if (s.getValue() <= 1 && s.getValue() >= 0 && t.getValue() >= 0)
                return true;
        }
        return false;
    }
    
    public boolean intersects(SegmentLine r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}
            
            if (s.getValue() <= 1 && s.getValue() >= 0 && t.getValue() >= 0 && t.getValue() <= 1)
                return true;
        }
        return false;
    }
    
    double distPointSegment (Vector A)
    {
        Vector P = new Vector(a);
        Vector d = new Vector(b).sub(P);
        
        Double t = d.dot(A.sub(P)) / d.dot(d);
        
        if(t <= 0)
            return A.sub(P).getModule();
        
        if(t < 1)
            return A.sub(P.add(d.scalarMult(t))).getModule();
        
        return A.sub(P.add(d)).getModule();    
    }
    
    public double angulo(Vector p){
        Vector a_v = new Vector(a);
        Vector b_v = new Vector(b);
        Vector v = b_v.sub(a_v);
        Vector d = p.sub(a_v);
        v = v.scalarMult(1/v.getModule());
        d = d.scalarMult(1/d.getModule());
        double auxuas = v.dot(d);
        double aux = Math.acos(v.dot(d));
        return Math.acos(v.dot(d));
    }
    
    /**
     * Muestra en pantalla la informacion del SegmentLine.
     */
    public void out()
    {
        System.out.println("Punto a: ");
        a.out();
        System.out.println("Punto b: ");
        b.out();
    }

}
