/**
 * Java Library for Geometric Algorithms subject
 *
 * @author Lidia Ortega, Alejandro Graciano
 * @version 1.0
 */
package algGeom.lib2D;

import Util.GeomMethods;
import com.sun.opengl.util.Animator;
import Util.RandomGen;
import static algGeom.lib2D.Circle.RelationCircleLine.INTERSECTS;
import static algGeom.lib2D.Circle.RelationCircleLine.TANGENTS;
import java.awt.Frame;
import java.awt.BorderLayout;

import javax.media.opengl.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.internal.misc.VM;

public class Test extends Frame implements GLEventListener {

    static int A = 0;
    static int B = 1;
    static int HEIGHT = 800;
    static int WIDTH = 800;
    static int POINT_CLOUD_VERT = 20;

    static GL gl; // interface to OpenGL
    static GLCanvas canvas; // drawable in a frame
    static GLCapabilities capabilities;

    static boolean visualizeAxis = false;
    static int exercise = A;
    static Animator animator;
    
    Runnable tri_animation;
    Thread tr;
    Semaphore smph;
    

    // Geometric data in memory
    // Exercise A 
    Point p;
    DrawPoint dp;

    PointCloud pc;
    DrawPointCloud dpc;
    
    TDelaunay dt1;
    DrawTDelaunay ddt1;
    
    TDelaunay dt2;
    DrawTDelaunay ddt2;

    SegmentLine s1;
    DrawSegment ds1;

    RayLine r1;
    DrawRay dr1;

    Line l1;
    DrawLine dl1;

    Vector v1;
    DrawVector dv1;

    Polygon pol;
    DrawPolygon dpol;

    Circle cir;
    DrawCircle dcir;

    // Exercise B
    Point p2;
    DrawPoint dp2;

    SegmentLine s2;
    DrawSegment ds2;

    RayLine r2;
    DrawRay dr2;

    Line l2;
    DrawLine dl2;

    Vector v2;
    DrawVector dv2;

    Circle cir2, cir3, cir4;
    DrawCircle dcir2, dcir3, dcir4;

    public Test(String title) {
        super(title);
        
        smph = new Semaphore(1);

        // 1. specify a drawable: canvas
        capabilities = new GLCapabilities();
        capabilities.setDoubleBuffered(false);
        canvas = new GLCanvas();

        // 2. listen to the events related to canvas: reshape
        canvas.addGLEventListener(this);

        // 3. add the canvas to fill the Frame container
        add(canvas, BorderLayout.CENTER);

        // 4. interface to OpenGL functions
        gl = canvas.getGL();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
//				animator.stop(); // stop animation
                System.exit(0);
            }
        });

        canvas.addKeyListener(new KeyListener() {
            long clock = 0;

            /**
             * Handle the key typed event from the text field.
             */
            public void keyTyped(KeyEvent e) {
//                        System.out.println(e + "KEY TYPED: ");
            }

            /**
             * Handle the key pressed event from the text field.
             */
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyChar()) {
                    case 'E':
                    case 'e':
                        visualizeAxis = !visualizeAxis;
                        canvas.repaint();
                        clock = e.getWhen();
                        break;
                    case 'P':
                    case 'p':
                        if (tr == null || !tr.isAlive())
                        {
                            tr = new Thread(tri_animation);
                            tr.start();
                        }
                        break;
                    case 'a':
                    case 'A':
                        exercise = A;
                        break;
                    case 'b':
                    case 'B':
                        exercise = B;
                        break;
                    case 27: // esc
                        System.exit(0);
                }

            }

            /**
             * Handle the key released event from the text field.
             */
            public void keyReleased(KeyEvent e) {
                clock = e.getWhen();
//                        System.out.println(e + "KEY RELEASED: ");
            }
        });
    }

    protected void initExerciseA() throws Exception {
//        RandomGen random = RandomGen.getInst();
//
//        pc = new PointCloud("test.cloud");
//        dpc = new DrawPointCloud(pc);
//
//        s1 = new SegmentLine(pc.getPoint(random.nextUInt() % pc.size()),
//                pc.getPoint(random.nextUInt() % pc.size()));
//        ds1 = new DrawSegment(s1);
//
//        r1 = new RayLine(pc.getPoint(random.nextUInt() % pc.size()),
//                pc.getPoint(random.nextUInt() % pc.size()));
//        dr1 = new DrawRay(r1);
//
//        l1 = new Line(pc.getPoint(random.nextUInt() % pc.size()),
//                pc.getPoint(random.nextUInt() % pc.size()));
//        dl1 = new DrawLine(l1);
//
//        // TO DO : Falta dibujar el poligono convexo cogiendo los puntos de mayor
//        // /menor abcisa/ordenada y a�adir un punto mas de la nube de forma que 
//        // siga siendo convexo
//        // TODO : Falta centrar el circulo
//        // Polygon
//        ArrayList< Point> coord = new ArrayList< Point>();
//        GeomMethods.get4Corners(coord, pc);
//        pol = GeomMethods.create4Polygon(coord.get(0), coord.get(1), coord.get(2), coord.get(3));
//        // pol = GeomMethods.addToConvex(pol, pc);
//        System.out.println(pol.convex());
//        dpol = new DrawPolygon(pol);
//
//        // Circle located at the most central point
//        Point center = pc.centralPoint();
//        cir = new Circle(center, center.distance(pc.getPoint(random.nextInt(pc.size() - 1))));
//        dcir = new DrawCircle(cir);

        dt1 = new TDelaunay(100);
        ddt1 = new DrawTDelaunay(dt1);
        System.out.println("NumLineas: " + dt1.getEdges().size());

    }

    protected void drawExerciseA() {

//        dpc.drawObjectC(gl, 1, 1, 1);
//
//        ds1.drawObjectC(gl, 1, 0, 0);
//        dr1.drawObjectC(gl, 0, 1, 0);
//        dl1.drawObjectC(gl, 0, 0, 1);
//
//        dpol.drawObjectC(gl, 1, 1, 0);
//
//        dcir.drawObjectC(gl, 0, 1, 1);

          ddt1.drawObjectC(gl, 1, 1, 1);

    }

    protected void initExerciseB() {
//        v1 = new Vector();
//        v2 = new Vector();
//
//        l2 = new Line(new Point(0, 0), new Point(1, 0));
//        dl2 = new DrawLine(l2);
//
//        r2 = new RayLine(new Point(-40, -40), new Point(60, 0));
//        dr2 = new DrawRay(r2);
//
//        cir2 = new Circle(new Point(20, -30), 20);
//        dcir2 = new DrawCircle(cir2);
//
//        s2 = new SegmentLine(new Point(40, -60), new Point(40, -5));
//        ds2 = new DrawSegment(s2);
//
//        p2 = new Point(-30, -5);
//        dp2 = new DrawPoint(p2);
//
//        cir3 = new Circle(new Point(20,-55), 5);
//        dcir3 = new DrawCircle(cir3);
//        
//        cir4 = new Circle(new Point(20, -70), 15);
//        dcir4 = new DrawCircle(cir4);
//        
//        System.out.println("Distancias: ");
//
//        System.out.println("P-A: ");
//        System.out.println(l2.distPointLine(new Vector(p2)));
//
//        System.out.println("P-B: ");
//        System.out.println(r2.distPointRay(new Vector(p2)));
//
//        System.out.println("P-C: ");
//        System.out.println(cir2.c.distance(new Vector(p2)) - cir2.r);
//
//        System.out.println("P-D: ");
//        System.out.println(s2.distPointSegment(new Vector(p2)));
//
//        System.out.println("Interseciones: ");
//
//        if (l2.intersects(r2, v1)) {
//            System.out.println("A-B: ");
//            v1.out();
//        }
//
//        if (cir2.intersects(r2, v1, v2) == INTERSECTS) {
//            System.out.println("B-C: - Intersecta");
//            v1.out();
//            v2.out();
//        }
//
//        if (cir2.intersects(r2, v1, v2) == TANGENTS) {
//            System.out.println("B-C: - Tangente");
//            v1.out();
//        }
//
//        if (r2.intersects(s2, v1)) {
//            System.out.println("B-D: ");
//            v1.out();
//        }
//
//        if (cir2.intersects(s2, v1, v2) == INTERSECTS) {
//            System.out.println("C-D: - Intersecta");
//            v1.out();
//            v2.out();
//        }
//
//        if (cir2.intersects(s2, v1, v2) == TANGENTS) {
//            System.out.println("C-D: - Tangente");
//            v1.out();
//        }
//        
//        System.out.println("Circulos: ");
//        
//        System.out.println("Circulo 1 y  C" );
//        System.out.println(cir2.circleRelation(cir3));
//        
//        System.out.println("Circulo 1 y  Circulo 2" );
//        System.out.println(cir3.circleRelation(cir4));

        final PointCloud pc = new PointCloud(50);
        
        final PointCloud initial_pc = new PointCloud(0);
        initial_pc.addPoint(pc.getPoint(0));
        initial_pc.addPoint(pc.getPoint(1));
        initial_pc.addPoint(pc.getPoint(2));
        
        ddt2 = new DrawTDelaunay();
        
        tri_animation = new Runnable() {
            public void run() {

                TDelaunay delaunay = new TDelaunay(initial_pc);

                ddt2 = new DrawTDelaunay(delaunay);
                
                for (int i = 3; i < pc.size(); ++i)
                {
                    try
                    {
                        Thread.sleep(600);
                        
                        smph.acquire();
                        delaunay.addPoint(pc.getPoint(i));
                        smph.release();
                        
                    } catch (InterruptedException ex)
                    {
                        Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        };
    }

    protected void drawExerciseB() {
//        dl2.drawObject(gl);
//
//        dr2.drawObject(gl);
//
//        dcir2.drawObject(gl);
//
//        ds2.drawObject(gl);
//
//        dp2.drawObject(gl);
//        
//        dcir3.drawObject(gl);
//        dcir4.drawObject(gl);

        ddt2.drawObjectC(gl, 1, 1, 1);
    }

    // called once for OpenGL initialization
    public void init(GLAutoDrawable drawable) {

        animator = new Animator(canvas);
        animator.start(); // start animator thread
        // display OpenGL and graphics system information
        System.out.println("INIT GL IS: " + gl.getClass().getName());
        System.err.println(drawable.getChosenGLCapabilities());
        System.err.println("GL_VENDOR: " + gl.glGetString(GL.GL_VENDOR));
        System.err.println("GL_RENDERER: " + gl.glGetString(GL.GL_RENDERER));
        System.err.println("GL_VERSION: " + gl.glGetString(GL.GL_VERSION));

        RandomGen random = RandomGen.getInst();
        //random.setSeed(5308170449555L);
        System.err.println("SEED: " + random.getSeed());

        try {

            initExerciseA();
            initExerciseB();

        } catch (Exception ex) {
            System.out.println("Error en el dibujado");
            ex.printStackTrace();
        }
        
        canvas.requestFocus();

    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
            int height) {

        Draw.WIDTH = WIDTH = width; // new width and height saved
        Draw.HEIGH = HEIGHT = height;
        //DEEP = deep;
        if (Draw.HEIGH < Draw.WIDTH) {
            Draw.WIDTH = Draw.HEIGH;
        }
        if (Draw.HEIGH > Draw.WIDTH) {
            Draw.HEIGH = Draw.WIDTH;
        }
        // 7. specify the drawing area (frame) coordinates
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(0, width, 0, height, -100, 100);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
        if (HEIGHT < WIDTH) {
            gl.glTranslatef((WIDTH - HEIGHT) / 2, 0, 0);
        }
        if (HEIGHT > WIDTH) {
            gl.glTranslatef(0, (HEIGHT - WIDTH) / 2, 0);
        }

    }

    // called for OpenGL rendering every reshape
    public void display(GLAutoDrawable drawable) {

        // limpiar la pantalla
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        /* El color de limpiado ser cero */
        gl.glClearDepth(1.0);
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        if (visualizeAxis) {
            DrawAxis ejes = new DrawAxis();
            ejes.drawObject(gl);
        }
        if (exercise == A) {
            drawExerciseA();
        }

        if (exercise == B) {
            try {
                smph.acquire();
                drawExerciseB();
                smph.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        gl.glFlush();
    }

    // called if display mode or device are changed
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
            boolean deviceChanged) {
    }

    public static void main(String[] args) {
        Draw.HEIGH = HEIGHT;
        Draw.WIDTH = WIDTH;
        Test frame = new Test("Prac1. Algoritmos Geometricos");
        frame.setSize(WIDTH, HEIGHT);
        frame.setVisible(true);
    }
}
