/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib2D;


import java.util.ArrayList;
import java.util.List;


import com.vividsolutions.jts.triangulate.DelaunayTriangulationBuilder;
import com.vividsolutions.jts.triangulate.IncrementalDelaunayTriangulator;
import com.vividsolutions.jts.triangulate.quadedge.Vertex;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Coordinate;




/**
 * 
 * @author lidia
 */
public class EjemploDelunay
{

    
  final static GeometryFactory geomFact = new GeometryFactory();
	
  final static double SIDE_LEN = 1.0;
  final static double BASE_OFFSET = 1000;
  
  List<DrawSegment> dss;

  List severalPoints () {
      
    List pts = new ArrayList();
		
    pts.add(new Coordinate (3,5));
    pts.add(new Coordinate (0,0));
    pts.add(new Coordinate (1,2));
    pts.add(new Coordinate (5,5));
    pts.add(new Coordinate (8,2));
    pts.add(new Coordinate (4,-1));
    pts.add(new Coordinate (4,3));
                
            
    return pts;
}
        
  
  
  public void run() {
    
    // Se genera una lista de puntos   
    List pts = severalPoints();
    System.out.println("# pts: " + pts.size());
    
    // se hace la triangulaci�n de Delaunay con esa lista de puntos 
    DelaunayTriangulationBuilder builder = new DelaunayTriangulationBuilder();
    builder.setSites(pts);
                
    // extraemos la geometr�a resultante como tri�ngulos 
    Geometry g = builder.getTriangles(geomFact);            
    List<Geometry> triangles = new ArrayList<Geometry>(g.getNumGeometries());
                
    // se crea un lista de tri�ngulos 
    for(int i = 0; i < g.getNumGeometries(); ++i) {
        triangles.add(g.getGeometryN(i));
    }
                
    // se muestra la lista de tri�ngulos             
    for(int i = 0; i < triangles.size(); ++i) {
        System.out.println(triangles.get(i));
    }
                
    // se han creado 6 tri�ngulos 
    System.out.println (" N�mero de geometr�as: " + g.getNumGeometries() );
                
    // a�adimos un nuevo punto a la triangulaci�n anterior 
    IncrementalDelaunayTriangulator idt = new IncrementalDelaunayTriangulator(builder.getSubdivision());            
    idt.insertSite(new Vertex (9,0));
                
    // el n�mero de tri�ngulos de ha incrementado a 7 
    g = builder.getTriangles(geomFact);                
    System.out.println (" N�mero de geometr�as: " + g.getNumGeometries() );
    

                
 }
	

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       EjemploDelunay test = new EjemploDelunay();
       test.run();
    }

}
