/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import algGeom.lib3D.Triangle3d;
import algGeom.lib3D.Vect3d;

/**
 *
 * @author Javier Mart�nez Cavanillas
 */
public class MollerTriAABB
{  
    private static void findMinMax(double x0, double x1, double x2, 
            MutableDouble min, MutableDouble max)
    {
        min.setValue(x0);
        max.setValue(x0);
        
        if (x1 < min.getValue()) min.setValue(x1);
        if (x1 > max.getValue()) max.setValue(x1);
        if (x2 < min.getValue()) min.setValue(x2);
        if (x2 > max.getValue()) max.setValue(x2);
    }
    
    private static boolean planeBoxOverlap(Vect3d normal, Vect3d vert, Vect3d maxbox)
    {
        int q;
        Vect3d vmin = new Vect3d(), vmax = new Vect3d();
        double v;
        
        v = vert.getX();
        if (normal.getX() > 0)
        {
            vmin.setX(-maxbox.getX() - v);
            vmax.setX( maxbox.getX() - v);
        } else {
            vmin.setX( maxbox.getX() - v);
            vmax.setX(-maxbox.getX() - v);
        }
        
        v = vert.getY();
        if (normal.getY() > 0)
        {
            vmin.setY(-maxbox.getY() - v);
            vmax.setY( maxbox.getY() - v);
        } else {
            vmin.setY( maxbox.getY() - v);
            vmax.setY(-maxbox.getY() - v);
        }
        
        v = vert.getZ();
        if (normal.getZ() > 0)
        {
            vmin.setZ(-maxbox.getZ() - v);
            vmax.setZ( maxbox.getZ() - v);
        } else {
            vmin.setZ( maxbox.getZ() - v);
            vmax.setZ(-maxbox.getZ() - v);
        }
        
        if (normal.dot(vmin) > 0) return false;
        if (normal.dot(vmax) >=  0) return true;
        
        return false;
    }
    
    /*======================== X-tests ========================*/
    private static boolean AXISTEST_X(double a, double b, double fa, double fb,
            Vect3d boxhalfsize, Vect3d v0, Vect3d v2)
    {
        double min, max, rad;
        double p0 = a * v0.getY() - b * v0.getZ();
        double p2 = a * v2.getY() - b * v2.getZ();
        
        if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;}
        
        rad = fa * boxhalfsize.getY() + fb * boxhalfsize.getZ();
        
        return !(min > rad || max < -rad);
    }
    
    /*======================== Y-tests ========================*/
    private static boolean AXISTEST_Y(double a, double b, double fa, double fb,
            Vect3d boxhalfsize, Vect3d v0, Vect3d v2)
    {
        double min, max, rad;
        double p0 = -a * v0.getX() + b * v0.getZ();
        double p2 = -a * v2.getX() + b * v2.getZ();
        
        if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;}
        
        rad = fa * boxhalfsize.getX() + fb * boxhalfsize.getZ();
        
        return !(min > rad || max < -rad);
    }
    
    /*======================== Z-tests ========================*/
    private static boolean AXISTEST_Z(double a, double b, double fa, double fb,
            Vect3d boxhalfsize, Vect3d v0, Vect3d v2)
    {
        double min, max, rad;
        double p0 = a * v0.getX() - b * v0.getY();
        double p2 = a * v2.getX() - b * v2.getY();
        
        if(p2<p0) {min=p2; max=p0;} else {min=p0; max=p2;}
        
        rad = fa * boxhalfsize.getX() + fb * boxhalfsize.getY();
        
        return !(min > rad || max < -rad);
    }
    
    public static boolean triBoxOverlap(Vect3d boxcenter, Vect3d boxhalfsize, Triangle3d tri)
    {
        Vect3d v0,v1, v2;   
        double fex, fey, fez;
        MutableDouble min = new MutableDouble(0), max = new MutableDouble(0);
        Vect3d normal, e0, e1, e2;
        
        v0 = tri.getA().sub(boxcenter);
        v1 = tri.getB().sub(boxcenter);
        v2 = tri.getC().sub(boxcenter);
        
        e0 = v1.sub(v0);
        e1 = v2.sub(v1);
        e2 = v0.sub(v2);
        
        fex = Math.abs(e0.getX());
        fey = Math.abs(e0.getY());
        fez = Math.abs(e0.getZ());
        
        if(!AXISTEST_X(e0.getZ(), e0.getY(), fez, fey, boxhalfsize, v0, v2)) return false;
        if(!AXISTEST_Y(e0.getZ(), e0.getX(), fez, fex, boxhalfsize, v0, v2)) return false;
        if(!AXISTEST_Z(e0.getY(), e0.getX(), fey, fex, boxhalfsize, v1, v2)) return false;
        
        fex = Math.abs(e1.getX());
        fey = Math.abs(e1.getY());
        fez = Math.abs(e1.getZ());
        
        if(!AXISTEST_X(e1.getZ(), e1.getY(), fez, fey, boxhalfsize, v0, v2)) return false;
        if(!AXISTEST_Y(e1.getZ(), e1.getX(), fez, fex, boxhalfsize, v0, v2)) return false;
        if(!AXISTEST_Z(e1.getY(), e1.getX(), fey, fex, boxhalfsize, v0, v1)) return false;
        
        fex = Math.abs(e2.getX());
        fey = Math.abs(e2.getY());
        fez = Math.abs(e2.getZ());
        
        if(!AXISTEST_X(e2.getZ(), e2.getY(), fez, fey, boxhalfsize, v0, v1)) return false;
        if(!AXISTEST_Y(e2.getZ(), e2.getX(), fez, fex, boxhalfsize, v0, v1)) return false;
        if(!AXISTEST_Z(e2.getY(), e2.getX(), fey, fex, boxhalfsize, v1, v2)) return false;
        
        findMinMax(v0.getX(), v1.getX(), v2.getX(), min, max);
        if(min.getValue() > boxhalfsize.getX() || max.getValue() < -boxhalfsize.getX()) return false;
        
        findMinMax(v0.getY(), v1.getY(), v2.getY(), min, max);
        if(min.getValue() > boxhalfsize.getY() || max.getValue() < -boxhalfsize.getY()) return false;
        
        findMinMax(v0.getZ(), v1.getZ(), v2.getZ(), min, max);
        if(min.getValue() > boxhalfsize.getZ() || max.getValue() < -boxhalfsize.getZ()) return false;
        
        normal = e0.xProduct(e1);
        
        return planeBoxOverlap(normal, v0, boxhalfsize);
    }
}
