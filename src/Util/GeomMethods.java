package Util;

import algGeom.lib2D.Point;
import algGeom.lib2D.PointCloud;
import algGeom.lib2D.Polygon;
import algGeom.lib2D.SegmentLine;
import algGeom.lib2D.Vector;
import algGeom.lib3D.Face;
import algGeom.lib3D.Plane;
import algGeom.lib3D.PointCloud3d;
import algGeom.lib3D.Segment3d;
import algGeom.lib3D.Triangle3d;
import algGeom.lib3D.TriangleMesh;
import algGeom.lib3D.Vect3d;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GeomMethods {

    public static boolean get4Corners(ArrayList< Point> a, PointCloud cloud) {
        if (cloud.size() < 4) {
            return false;
        }
        double X_MAX, X_MIN, Y_MAX, Y_MIN;
        X_MAX = Y_MAX = Double.MIN_VALUE;
        X_MIN = Y_MIN = Double.MAX_VALUE;

        Point p;
        for (int i = 0; i < 4; i++) {
            a.add(new Point());
        }
        for (int i = 0; i < cloud.size(); i++) {
            p = cloud.getPoint(i);
            if (p.getX() > X_MAX) {
                a.set(0, p);
                X_MAX = p.getX();
            }
            if (p.getX() < X_MIN) {
                a.set(2, p);
                X_MIN = p.getX();
            }

            if (p.getY() > Y_MAX) {
                a.set(1, p);
                Y_MAX = p.getY();
            }
            if (p.getY() < Y_MIN) {
                a.set(3, p);
                Y_MIN = p.getY();
            }
        }

        return true;
    }

    public static Polygon create4Polygon(Point a, Point b, Point c, Point d) {
        Polygon pol = new Polygon();

        Point[] point = new Point[4];
        point[0] = a;
        point[1] = b;
        point[2] = c;
        point[3] = d;

        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                if (i != j) {
                    int kC = 0, kI = -1;
                    for (int k = 0; k < 4; ++k) {
                        if (i != k && j != k && point[k].left(point[i], point[j])) {
                            ++kC;
                            kI = k;
                        }
                    }
                    if (kC == 1) {
                        pol.add(point[j]);
                        pol.add(point[kI]);
                        pol.add(point[i]);
                        pol.add(point[6 - i - j - kI]);
                        return pol;
                    }
                }
            }
        }

        return pol;
    }

    @SuppressWarnings("empty-statement")
    public static TriangleMesh CH_GiftWrapping(PointCloud3d cloud) {
        // 0
        List<Triangle3d> triangles = new ArrayList<Triangle3d>();

        // 1 - 2
        LinkedList<Segment3d> boundaryCH = new LinkedList();
        LinkedList<Vect3d> boundaryCHAux = new LinkedList();
        HashSet<Vect3d> PointsInCH = new HashSet();

        // 3
        PointCloud3d R = new PointCloud3d();
        for (Vect3d point : cloud.getPoints()) {
            Vect3d v = new Vect3d(point);
            R.addPoint(v);
        }

        // 4
        Vect3d A = R.getPoint(0);
        for (int i = 1; i < R.getPoints().size(); i++) {
            Vect3d v = R.getPoint(i);
            if (v.getY() < A.getY()) {
                A = v;
            }
        }

        // 5
        Vect3d B = R.getPoint(0);
        SegmentLine h = new SegmentLine(A.getX(), A.getY(), A.getX() + 1, A.getY());
        double angl = Double.MAX_VALUE;
        for (int i = 0; i < R.getPoints().size(); i++) {
            Vect3d v = R.getPoint(i);
            double anglAux = h.angulo(new Vector(v.getX(), v.getY()));
            if (anglAux < angl) {
                angl = anglAux;
                B = v;
            }
        }

        // 6
        Vect3d C = null;
        boolean exito;
        for (int i = 0; i < R.getPoints().size(); ++i) {
            exito = false;
            C = R.getPoint(i);
            if (!C.equals(A) && !C.equals(B)) {
                exito = true;
                Triangle3d tCase = new Triangle3d(A, B, C);
                for (int j = 0; j < R.getPoints().size(); j++) {
                    Vect3d point = R.getPoint(j);
                    if (tCase.classify(point) == Triangle3d.PointPosition.POSITIVE) {
                        exito = false;
                        break;
                    }
                }
                if (exito) {
                    break;
                }
            }
        }

        // 7
        boundaryCH.add(new Segment3d(B, C));
        boundaryCHAux.add(A);
        boundaryCH.add(new Segment3d(C, A));
        boundaryCHAux.add(B);
        boundaryCH.add(new Segment3d(A, B));
        boundaryCHAux.add(C);
        // 8
        PointsInCH.add(A);
        PointsInCH.add(B);
        PointsInCH.add(C);

        // 9
        triangles.add(new Triangle3d(A, B, C));

        //10
        while (!boundaryCH.isEmpty()) {
            //a
            Segment3d DE = boundaryCH.pop();
            A = DE.getOrigin();
            B = DE.getDestination();
            C = boundaryCHAux.pop();

            Vect3d V = null;
            for (int i = 0; i < R.getPoints().size(); ++i) {
                exito = false;
                V = R.getPoint(i);
                if (!V.equals(C) && !V.equals(B) && !V.equals(A)) {
                    exito = true;
                    Triangle3d tCase = new Triangle3d(B, A, V);
                    for (int j = 0; j < R.getPoints().size(); j++) {
                        Vect3d point = R.getPoint(j);
                        if (tCase.classify(point) == Triangle3d.PointPosition.POSITIVE) {
                            exito = false;
                            break;
                        }
                    }
                    if (exito) {
                        break;
                    }
                    exito = true;
                    for (int j = 0; j < R.getPoints().size(); j++) {
                        Vect3d point = R.getPoint(j);
                        if (tCase.classify(point) == Triangle3d.PointPosition.NEGATIVE) {
                            exito = false;
                            break;
                        }
                    }
                    if (exito) {
                        break;
                    }
                }
            }
            
            triangles.add(new Triangle3d(B, A, V));
            
            //Para que no se quede atascado en la envolvente de los modelos 3d por tener puntos complanarios
            if(triangles.size() == 1000)
                break;
            
            Segment3d AV = new Segment3d(A, V);
            Segment3d BV = new Segment3d(B, V);
            //d
            if (!PointsInCH.contains(V)) {
                //e
                PointsInCH.add(V);
                boundaryCH.add(AV);
                boundaryCHAux.add(B);
                boundaryCH.add(BV);
                boundaryCHAux.add(A);
            } else {
                // f
                boolean c_AV = boundaryCH.contains(AV);
                boolean c_BV = boundaryCH.contains(BV);
                if (c_AV && !c_BV) {
                    boundaryCHAux.remove(boundaryCH.indexOf(AV));
                    boundaryCH.remove(AV);
                    boundaryCH.add(BV);
                    boundaryCHAux.add(A);
                } else if (!c_AV && c_BV) {
                    boundaryCHAux.remove(boundaryCH.indexOf(BV));
                    boundaryCH.remove(BV);
                    boundaryCH.add(AV);
                    boundaryCHAux.add(B);
                } else if (c_AV && c_BV) {
                    boundaryCHAux.remove(boundaryCH.indexOf(AV));
                    boundaryCH.remove(AV);
                    boundaryCHAux.remove(boundaryCH.indexOf(BV));
                    boundaryCH.remove(BV);
                } else {
                    boundaryCH.add(AV);
                    boundaryCHAux.add(B);
                    boundaryCH.add(BV);
                    boundaryCHAux.add(A);
                }
            }
        }
        return new TriangleMesh(triangles);
    }
}
